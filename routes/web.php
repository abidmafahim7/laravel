<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'Home@index' );

Route::post('/add', 'Home@ins' )->name('inshome');

Route::prefix('v1')->group(function() {
	Route::prefix('auth')->group(function() {
		Route::get('register', 'Auth\RegisterController@showregistrationform')->name('reg1');
		Route::post('register', 'Auth\RegisterController@register')->name('reg');
		Route::get('logout', 'Auth\LoginController@logout');
		Route::get('login', 'Auth\LoginController@showloginform')->name('log');
		Route::post('login', function() {
			session([
				'status' => "logged in"
			]);
			return redirect()->action('Auth\LoginController@login');
		})->name('log');
	});
	Route::get('board', 'boardController@index');
	Route::post('board', 'boardController@create')->name('makeboard');
});

